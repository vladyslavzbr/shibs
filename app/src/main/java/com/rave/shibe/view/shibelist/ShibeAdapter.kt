package com.rave.shibe.view.shibelist

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.shibe.databinding.ItemShibeBinding
import com.rave.shibe.model.ShibeRepo
import com.rave.shibe.model.local.ShibeDatabase
import com.rave.shibe.model.local.dao.ShibeDao
import com.rave.shibe.model.local.entity.Shibe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeAdapter(
    private val shibes: List<Shibe>
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadShibeImage(url)
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe) {

            if (shibe.favorited) {
                binding.imageView.setBackgroundColor(Color.rgb(255,255,255))
            }

            binding.ivShibe.load(shibe.url)

            binding.ivShibe.setOnClickListener {
                val repo = ShibeRepo(it.context)
                if(!shibe.favorited) {
                    binding.imageView.setBackgroundColor(Color.rgb(255, 255, 255))
                    shibe.favorited = true
                    CoroutineScope(Dispatchers.IO).launch {
                        repo.shibeDao.update(shibe)
                    }
                } else {
                    binding.imageView.setBackgroundColor(Color.TRANSPARENT)
                    shibe.favorited = false
                    CoroutineScope(Dispatchers.IO).launch {
                        repo.run { shibeDao.update(shibe) }
                    }
                }

            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}

